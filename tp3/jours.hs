
-- TODO Jour

data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables  = length . filter (not . estWeekend)

compterOuvrables2 :: [Jour] -> Int
compterOuvrables2 [] = 0
compterOuvrables2 (h:t)
    | estWeekend h = compterOuvrables2 t
    | otherwise = 1 + compterOuvrables2 t

main :: IO ()
main = do
    print $ estWeekend Lundi
    print $ estWeekend Samedi
    print $ compterOuvrables [Lundi, Mardi, Dimanche]
    print $ compterOuvrables2 [Lundi, Mardi, Dimanche]


