
-- TODO User
-- Voir derving show
data User = User
    { _nom   :: String,
     _prenom :: String,
     _age    :: Int
    } deriving Show

showUser :: User -> String
showUser (User nom prenom age) = "Nom : " ++ nom ++ ", Prenom : " ++ prenom ++ ", Age" ++ show age ++ "."
-- On peux aussi faire : showUser user = "Nom : " ++ _nom user ++ ", Prenom : " ++ _prenom user ++ ", Age" ++ show (_age user) ++ "."

incAge :: User -> User
incAge u = u { _age = _age u + 1}

main :: IO ()
main = do
    let u1 = User "Moi" "Oui" 42
    print $ showUser u1

