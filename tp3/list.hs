
-- TODO List

data List a = Nil | Cons a (List a) deriving Show

-- data List2 a = Empty | Cons2 a (List2 a)

sumList :: Num a => List a -> a 
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs

concatList :: List a -> List a -> List a
concatList Nil ys = ys
concatList (Cons x xs) ys  = Cons x (concatList xs ys)

toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x:toHaskell xs

fromHaskell :: [a] -> List a
fromHaskell [] = Nil
fromHaskell (x:xs) = Cons x (fromHaskell xs)

myShowList :: Show a => List a -> [Char]
myShowList Nil = ""
myShowList (Cons x xs) = show x ++ " " ++ myShowList xs

main :: IO ()
main = do
    print ((Cons 1 (Cons 2 Nil)) :: List Int)
    print ((Cons True(Cons False Nil)) :: List Bool)
    print (sumList (Cons 1(Cons 2 Nil)))
    print $ concatList (Cons "Foo" (Cons "Bar" Nil)) (Cons "Foo" (Cons "Bar" Nil))
    print $ toHaskell (Cons "foo" (Cons "Bar" Nil))
    print $ fromHaskell ["Foo", "Bar"]

