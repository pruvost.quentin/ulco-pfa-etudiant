fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (h:t)
    | mod h 15 == 0 = "FizzBuzz":fizzbuzz1 t
    | mod h 5 == 0 = "Buzz":fizzbuzz1 t
    | mod h 3 == 0 = "Fizz":fizzbuzz1 t
    | otherwise = (show h):fizzbuzz1 t

fizzBuzz3 n | mod n 15 == 0 = "FizzBuzz"
            | mod n 5 == 0 = "Buzz"
            | mod n 3 == 0 = "Fizz"
            | otherwise = (show n)

-- Possible de remplacer n par une liste []
fizzBuzz2 n = fizzBuzzPrime [] n -- [1..15]
fizzBuzzPrime acc 0 = acc
fizzBuzzPrime acc n | mod n 15 == 0 = fizzBuzzPrime ("FizzBuzz":acc) (n-1)
                    | mod n 5 == 0 = fizzBuzzPrime ("Buzz":acc) (n-1)
                    | mod n 3 == 0 = fizzBuzzPrime ("Fizz":acc) (n-1)
                    | otherwise = fizzBuzzPrime ((show n):acc) (n-1)

main :: IO ()
main = do
    print $ fizzbuzz1 [1..15]
    let fizzbuzz = map fizzBuzz3 [1..30]
    print $ take 15 fizzbuzz
    print $ fizzBuzz2 15

