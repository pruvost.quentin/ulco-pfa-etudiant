import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

readInt :: Int -> IO()
readInt entree = do
    putStr $ "Sasie " ++ show entree ++ " : "
    hFlush stdout
    value <- getLine
    -- let mx = readMaybe value :: Maybe Int
    case readMaybe value :: Maybe Int of
        Just x -> putStrLn $ "Vous avez sasi l'entier " ++ show x
        Nothing ->putStrLn "Sasie invalide"

main :: IO ()
main = forM_ [1..4] readInt

