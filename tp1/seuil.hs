seuilInt :: Integer -> Integer -> Integer ->Integer
seuilInt a b x
    | x > b = b
    | x < a = a
    | otherwise = x


seuilTuple :: (Integer, Integer) -> Integer -> Integer
seuilTuple (a, b) x
    | x > b = b
    | x < a = a
    | otherwise = x

main :: IO ()
main = do
    print $ seuilInt 1 10 0
    print $ seuilInt 1 10 7
    print $ seuilInt 1 10 42

    print $ seuilTuple (1,10) 0
    print $ seuilTuple (1,10) 7
    print $ seuilTuple (1,10) 42

