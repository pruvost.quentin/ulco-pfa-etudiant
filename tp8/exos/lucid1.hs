{-# LANGUAGE OverloadedStrings #-}

import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T
import Lucid

monH2 :: Html ()
monH2 = h2_ "tete"

monHtml :: T.Text -> Html ()
monHtml txt = do
    h1_ "hello"
    monH2
    p_ "world"
    div_ $ p_ "toto"
    div_ $ do
        p_ "tata"
        p_ $ toHtml txt

main :: IO ()
main = T.putStrLn $ renderText $ monHtml "tutu" 