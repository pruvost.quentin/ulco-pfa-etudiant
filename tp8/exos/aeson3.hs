{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson
import GHC.Generics

import qualified Data.Text.Lazy as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , adress       :: Adress
    } deriving (Generic, Show)

instance ToJSON Person

data Adress = Adress
    {
        number :: Int
      , road :: T.Text
      , zipcode :: Int
      , city :: T.Text
    } deriving (Generic, Show)

instance ToJSON Adress

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Adress 42 "Pont vieux" 43000 "Espaly")
    , Person "Haskell" "Curry" 1900 (Adress 42 "Pont vieux" 43000 "Espaly")
    ]

main :: IO ()
main = do
    encodeFile "out-aesons3.json" persons