{-# LANGUAGE OverloadedStrings #-}

-- import Data.Text.Lazy as T
-- import Data.Text.Lazy.IO as T
import Lucid


main :: IO ()
main = renderToFile "lucid2.html" maPage
maPage :: Html ()
maPage = do
    doctype_
    head_ $ do 
        meta_ [charset_ "utf-8"]
    html_ $ body_ $ do
        h1_ "hello"
        img_ [src_ "toto.png"]
        p_ $ do
            "this is "
            a_ [href_ "toto.png"] "a link"