{-# LANGUAGE OverloadedStrings #-}

import ViewAlbum
import Site

import Data.Aeson

main :: IO ()
main = do
    mSites <- decodeFileStrict "data/genalbum.json"
    case mSites of
        Nothing -> putStrLn "Loading failded"
        Just sites -> mapM_ print (sites :: [Site])

