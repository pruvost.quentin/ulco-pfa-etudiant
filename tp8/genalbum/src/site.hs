module Site where

data Site = Site
    {
        imgs :: [String]
        , url :: String   
    } deriving (Generic, Show)