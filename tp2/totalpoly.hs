
safeTailString :: String -> String
safeTailString "" = ""
safeTailString (_:t) = t

safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (x:_) = Just x

safeTail :: [a] -> [a]
safeTail [ ]= []
safeTail (_:t) = t

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

main :: IO ()
main = do
    print $ safeTailString "foobar"
    print $ safeHeadString "foobar"
    print $ safeTail "foobar"
    print $ safeHead "foobar"
