import Data.List

threshList :: Ord a => a -> [a] -> [a]
threshList x xs = map(min x) xs

selectList :: Ord a => a -> [a] -> [a]
selectList s xs = filter (<s) xs

maxList :: Ord a => [a] -> a
maxList xs = foldr1 max xs

-- map => retrun le meme nb d'elem
-- filter => return meme nb d'elem ou <
-- foldr => return un seul elem (on part de la droite)

main :: IO ()
main = do
    print $ threshList 3 [1..10::Int]
    print $ selectList 3 [1..10::Int]
    print $ maxList [1..10]
