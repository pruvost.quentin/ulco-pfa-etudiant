
{-# LANGUAGE DeriveGeneric #-}

module Model where
    
import qualified Data.Text.Lazy as L
import Data.Aeson
import GHC.Generics

data Rider = Rider
    {
        _name :: L.Text
        , _images :: [L.Text]
    } deriving Generic
    
instance ToJSON Rider