
{-# LANGUAGE OverloadedStrings #-}

module View where
    

import Lucid
import qualified Data.Text.Lazy as L

import Model

indexPage :: [Rider] -> L.Text
indexPage riders = renderText $ do
    doctype_
    html_ $ do
        meta_ [charset_ "utf-8"]
        title_ "ptivelo"
    body_ $ do
        h1_ "ptivelo"
        mapM_ mkRider riders

mkRider :: Rider -> Html ()
mkRider rider = div_  $ do
    {-h2_ $ toHtml $ _name rider
    mapM_ mkImg (_images rider)-}
    p_ $ toHtml (_name rider)

{-
mkImg :: [L.Text] -> Html ()
mkImg img = div_ $ do
    img_ [src_ (L.toStrict img), style_ "height:150px"]-}