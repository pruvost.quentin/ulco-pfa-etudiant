
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Maybe (fromMaybe)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Web.Scotty
import Data.Aeson hiding (json)
import GHC.Generics
import Lucid
import TextShow (showtl)

import qualified Data.Text.Lazy as L

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving Generic
    
instance ToJSON Person

myPage :: Html ()
myPage = do 
    ul_ $ do
        mkRoute "route1"
        li_ $ a_ [href_ "/route1/route2"] "route1/route2"
        mkRoute "html1"
        mkRoute "json1"
        mkRoute "json2"
        mkRoute "add1/40/2"
        mkRoute "add2?x=2&y=40"
        mkRoute "index"
    img_ [src_ "bob.png"]

mkRoute :: L.Text -> Html ()
mkRoute txt = li_ $ a_ [href_ (L.toStrict txt)] $ toHtml txt

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  middleware $ staticPolicy $ addBase "static"
  get "/" $ html $ renderText myPage
  get "/route1" $ text "this is route 1"
  get "/route1/route2" $ text "this is route1/route2"
  get "/json1" $ json $ L.pack "this is json"
  get "/html1" $ html "<h1>Ceci est du HTML</h1>"
  get "/json2" $ json (Person "toto" 42)
  get "add1/:x/:y" $ do
      x <- param "x"
      y <- param "y"
      text $ showtl (x+y :: Int)
  get "/add2" $ do
      x <- param "x" `rescue` (\_ ->return 0)
      y <- param "y" `rescue` (\_ ->return 0)
      text $ showtl (x+y :: Int)
  get "/index" $ redirect "/"